#!/bin/bash
# Aegir platform directory:
CheckDirectory="/var/aegir/platforms"

# Setup find correctly:
export IFS=$'\n'

## find all platform directories
for path in $CheckDirectory/*; do
    [ -d "${path}" ] || continue # if not a directory, skip
    dirname="$(basename "${path}")"
    
    # Skip directory Makefiles
    if [ $dirname == 'Makefiles' ]; then
            echo "Skip makefiles directory"
            continue
    fi

    read -p "Do you want to change permissions for $dirname (y/N)" -n 1 -r
    echo    # (optional) move to a new line
    #echo You selected $REPLY
    if [[ $REPLY =~ ^[yY]$ ]]; then
        echo "Change owner for all subdirs in $dirname"
        #echo chown aegir:aegir $CheckDirectory/$dirname -R
        sudo chown aegir:aegir $CheckDirectory/$dirname -R
        
        echo "Change permission for all subdirs in $dirname"
        #echo sudo chmod 755 for all directories
        for i in $(find $CheckDirectory/$dirname -type d); do 
            #echo "change permission for directory: $i"
            sudo chmod 755 $i
        done

        echo "Change permissions for all files in $dirname"
        #echo sudo chmod 544 for all files
        for i in $(find $CheckDirectory/$dirname -type f); do
            #echo "Change permission for file: $i"
            sudo chmod 644 $i
        done

        ## Finished setting permission for all files.
        echo "Finished setting permissions for all files."
         
        ## Continue with site files:
        for path in $CheckDirectory/$dirname/sites/*; do
            [ -d "${path}" ] || continue # if not a directory, skip
            sitename="$(basename "${path}")"
            #if [ $sitename == 'all' ]; then
            #    echo "Skip all-directory"
            #    continue
            #fi
            
            if [ -f "$CheckDirectory/$dirname/sites/$sitename/drushrc.php" ]; then
                echo "Fix drushrc.php for $sitename"
                sudo chmod 400 $CheckDirectory/$dirname/sites/$sitename/drushrc.php
            else
                echo File drushrc.php does not exist. It will be created by aegir.
            fi
            
            if [ -f "$CheckDirectory/$dirname/sites/$sitename/settings.php" ]; then
                echo "fix settings.php for $sitename"
                sudo chmod 440 $CheckDirectory/$dirname/sites/$sitename/settings.php
                sudo chown aegir:www-data $CheckDirectory/$dirname/sites/$sitename/settings.php
            fi

            CHECK_DIR=$CheckDirectory/$dirname/sites/$sitename/libraries
            if [ -d "$CHECK_DIR" ]; then
                echo "Change owner for $CHECK_DIR"
                #echo chown aegir:aegir $CHECK_DIR
                sudo chown aegir:aegir $CHECK_DIR
        
                echo "Change permission for $CHECK_DIR"
                #echo sudo chmod 2755 $CHECK_DIR
                for i in $(find $CHECK_DIR -type d); do 
                    #echo "change permission for directory: $i"
                    sudo chmod 2775 $i
                done

                echo "Change permissions for files in $CHECK_DIR"
                #echo sudo chmod 644 for all files
                for i in $(find $CHECK_DIR -type f); do
                    #echo "Change permission for file: $i"
                    sudo chmod 644 $i
                done
            else
                echo Directory libraries does not exists for $sitename. Thats OK.
            fi
            
            CHECK_DIR=$CheckDirectory/$dirname/sites/$sitename/modules
            if [ -d "$CHECK_DIR" ]; then
                echo "Change owner for $CHECK_DIR"
                #echo chown aegir:aegir $CHECK_DIR
                sudo chown aegir:aegir $CHECK_DIR
        
                echo "Change permission for $CHECK_DIR"
                #echo sudo chmod 2755 $CHECK_DIR
                for i in $(find $CHECK_DIR -type d); do 
                    #echo "change permission for directory: $i"
                    sudo chmod 2775 $i
                done

                echo "Change permissions for files in $CHECK_DIR"
                #echo sudo chmod 644 for all files
                for i in $(find $CHECK_DIR -type f); do
                    #echo "Change permission for file: $i"
                    sudo chmod 644 $i
                done
            else
                echo Directory modules does not exists for $sitename. Thats OK.
            fi
            
            CHECK_DIR=$CheckDirectory/$dirname/sites/$sitename/themes
            if [ -d "$CHECK_DIR" ]; then
                echo "Change owner for $CHECK_DIR"
                #echo chown aegir:aegir $CHECK_DIR
                sudo chown aegir:aegir $CHECK_DIR
    
                echo "Change permission for $CHECK_DIR"
                #echo sudo chmod 2775 $CHECK_DIR
                for i in $(find $CHECK_DIR -type d); do 
                    #echo "change permission for directory: $i"
                    sudo chmod 2775 $i
                done

                echo "Change permissions for files in $CHECK_DIR"
                #echo sudo chmod 644 for all files
                for i in $(find $CHECK_DIR -type f); do
                    #echo "Change permission for file: $i"
                    sudo chmod 644 $i
                done
            else
                echo Directory themes does not exists for $sitename. Thats OK.
            fi
            
            CHECK_DIR=$CheckDirectory/$dirname/sites/$sitename/files
            if [ -d "$CHECK_DIR" ]; then
            
                echo "Change owner for $CHECK_DIR/*"
                #echo sudo chown -R www-data:www-data $CHECK_DIR/
                sudo chown -R aegir:www-data $CHECK_DIR/

                echo "Change permission for $CHECK_DIR/*"
                #echo sudo chmod -R 2755 $CHECK_DIR/
                sudo chmod 2775 -R $CHECK_DIR/
            
                echo "Change owner for $CHECK_DIR"
                #echo sudo chown aegir:www-data $CHECK_DIR
                sudo chown aegir:www-data $CHECK_DIR
    
                echo "Change permission for $CHECK_DIR"
                #echo sudo chmod 2770 $CHECK_DIR
                sudo chmod 2770 $CHECK_DIR
                
            else
                echo Directory files does not exists for $sitename. Thats not normal...
            fi
            
            CHECK_DIR=$CheckDirectory/$dirname/sites/$sitename/private
            if [ -d "$CHECK_DIR" ]; then
                echo "Change owner for $CHECK_DIR/*"
                #echo sudo chown -R www-data:www-data $CHECK_DIR/
                sudo chown -R aegir:www-data $CHECK_DIR/

                echo "Change permission for $CHECK_DIR/*"
                #echo sudo chmod -R 2755 $CHECK_DIR/
                sudo chmod 2775 -R $CHECK_DIR/
            
                echo "Change owner for $CHECK_DIR"
                #echo sudo chown aegir:www-data $CHECK_DIR
                sudo chown aegir:www-data $CHECK_DIR
    
                echo "Change permission for $CHECK_DIR"
                #echo sudo chmod 2770 $CHECK_DIR
                sudo chmod 2770 $CHECK_DIR
            else
                echo Directory private does not exists for $sitename.
            fi
        done
    else
        echo skipping $dirname...
        continue
    fi
done

