# README #

I created this script when I was tired of manually fixing file-permissions on ægir-platforms.

### How to use ###

#### Download ####
Clone with git:

```
#!bash

git clone https://bitbucket.org/audub/aegir_permissions.git

```

#### Run script ####

```
#!bash

cd aegir_permissions
sudo ./fix_aegir_perm.sh
```


The script will ask you which directories to fix permissions for.